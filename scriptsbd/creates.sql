SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `projeto` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `projeto` ;

-- -----------------------------------------------------
-- Table `projeto`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`status` (
  `idstatus` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idstatus`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`setores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`setores` (
  `idsetor` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomesetor` VARCHAR(45) NULL,
  PRIMARY KEY (`idsetor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`unidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`unidades` (
  `idunidade` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numunidade` INT(11) NULL,
  `nomeunidade` VARCHAR(200) NULL,
  PRIMARY KEY (`idunidade`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`funcionarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`funcionarios` (
  `idfuncionario` INT NOT NULL AUTO_INCREMENT,
  `senha` VARCHAR(45) NULL,
  `nome` VARCHAR(100) NULL,
  `matricula` VARCHAR(45) NULL,
  `tipo` VARCHAR(100) NULL,
  PRIMARY KEY (`idfuncionario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`solicitantes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`solicitantes` (
  `idsolicitante` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `matricula` VARCHAR(20) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `tipo` INT NOT NULL,
  PRIMARY KEY (`idsolicitante`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`servicos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`servicos` (
  `idservico` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sla` TIME NOT NULL,
  `grauseveridade` INT NOT NULL,
  `descricao` VARCHAR(50) NOT NULL,
  `setores_idsetor` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idservico`, `setores_idsetor`),
  INDEX `fk_servicos_setores_idx` (`setores_idsetor` ASC),
  CONSTRAINT `fk_servicos_setores`
    FOREIGN KEY (`setores_idsetor`)
    REFERENCES `projeto`.`setores` (`idsetor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`andares`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`andares` (
  `idandar` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `numandar` INT NOT NULL,
  `unidades_idunidade` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idandar`, `unidades_idunidade`),
  INDEX `fk_andares_unidades1_idx` (`unidades_idunidade` ASC),
  CONSTRAINT `fk_andares_unidades1`
    FOREIGN KEY (`unidades_idunidade`)
    REFERENCES `projeto`.`unidades` (`idunidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`ambientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`ambientes` (
  `idambiente` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL,
  `andares_idandar` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idambiente`, `andares_idandar`),
  INDEX `fk_ambientes_andares1_idx` (`andares_idandar` ASC),
  CONSTRAINT `fk_ambientes_andares1`
    FOREIGN KEY (`andares_idandar`)
    REFERENCES `projeto`.`andares` (`idandar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`chamados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`chamados` (
  `idchamado` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_idstatus` INT UNSIGNED NOT NULL,
  `servicos_idservico` INT UNSIGNED NOT NULL,
  `ambientes_idambiente` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idchamado`, `status_idstatus`, `servicos_idservico`, `ambientes_idambiente`),
  INDEX `fk_chamados_status1_idx` (`status_idstatus` ASC),
  INDEX `fk_chamados_servicos1_idx` (`servicos_idservico` ASC),
  INDEX `fk_chamados_ambientes1_idx` (`ambientes_idambiente` ASC),
  CONSTRAINT `fk_chamados_status1`
    FOREIGN KEY (`status_idstatus`)
    REFERENCES `projeto`.`status` (`idstatus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamados_servicos1`
    FOREIGN KEY (`servicos_idservico`)
    REFERENCES `projeto`.`servicos` (`idservico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamados_ambientes1`
    FOREIGN KEY (`ambientes_idambiente`)
    REFERENCES `projeto`.`ambientes` (`idambiente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`chamadosaux`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`chamadosaux` (
  `idchamadoaux` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(200) NOT NULL,
  `horafim` DATETIME NULL,
  `principal` TINYINT(1) NOT NULL,
  `horainicio` DATETIME NOT NULL,
  `imagem` VARCHAR(250) NULL,
  `chamados_idchamado` INT UNSIGNED NOT NULL,
  `solicitantes_idsolicitante` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idchamadoaux`, `chamados_idchamado`, `solicitantes_idsolicitante`),
  INDEX `fk_chamadosaux_chamados1_idx` (`chamados_idchamado` ASC),
  INDEX `fk_chamadosaux_solicitantes1_idx` (`solicitantes_idsolicitante` ASC),
  CONSTRAINT `fk_chamadosaux_chamados1`
    FOREIGN KEY (`chamados_idchamado`)
    REFERENCES `projeto`.`chamados` (`idchamado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chamadosaux_solicitantes1`
    FOREIGN KEY (`solicitantes_idsolicitante`)
    REFERENCES `projeto`.`solicitantes` (`idsolicitante`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`avaliacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`avaliacoes` (
  `idavaliacao` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(200) NULL,
  `avaliacao` INT NULL,
  `chamadosaux_idchamadoaux` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idavaliacao`, `chamadosaux_idchamadoaux`),
  INDEX `fk_avaliacoes_chamadosaux1_idx` (`chamadosaux_idchamadoaux` ASC),
  CONSTRAINT `fk_avaliacoes_chamadosaux1`
    FOREIGN KEY (`chamadosaux_idchamadoaux`)
    REFERENCES `projeto`.`chamadosaux` (`idchamadoaux`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projeto`.`locacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projeto`.`locacoes` (
  `funcionarios_idfuncionario` INT NOT NULL,
  `chamados_idchamado` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`funcionarios_idfuncionario`, `chamados_idchamado`),
  INDEX `fk_funcionarios_has_chamados_chamados1_idx` (`chamados_idchamado` ASC),
  INDEX `fk_funcionarios_has_chamados_funcionarios1_idx` (`funcionarios_idfuncionario` ASC),
  CONSTRAINT `fk_funcionarios_has_chamados_funcionarios1`
    FOREIGN KEY (`funcionarios_idfuncionario`)
    REFERENCES `projeto`.`funcionarios` (`idfuncionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_funcionarios_has_chamados_chamados1`
    FOREIGN KEY (`chamados_idchamado`)
    REFERENCES `projeto`.`chamados` (`idchamado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
