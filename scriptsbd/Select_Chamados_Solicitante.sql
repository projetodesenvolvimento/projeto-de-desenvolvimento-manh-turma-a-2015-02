SELECT chamadosaux.chamados_idchamado AS chamado,
       servicos.descricao AS servicos_descricao,
       (select descricao from status where idstatus = chamados.status_idstatus) AS status,
       solicitantes.matricula AS matricula, 
       (select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
       andares.numandar AS andar, 
       ambientes.descricao AS ambientes_descricao, 
       setores.nomesetor AS setor,       
       chamadosaux.descricao AS chamadosaux_descricao,
       solicitantes.nome,
       solicitantes.idsolicitante
FROM solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados 
WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
AND chamadosaux.chamados_idchamado = chamados.idchamado
AND servicos.setores_idsetor = setores.idsetor
AND unidades.idunidade = andares.unidades_idunidade
AND ambientes.andares_idandar = andares.idandar
AND chamados.ambientes_idambiente = ambientes.idambiente
AND chamados.servicos_idservico = servicos.idservico
AND chamados.status_idstatus = (select idstatus from status where status.descricao = 'Em aberto');


SELECT chamadosaux.chamados_idchamado AS chamado,
servicos.descricao AS servicos_descricao,
(select descricao from status where idstatus = chamados.status_idstatus) AS status,
solicitantes.matricula AS matricula, 
(select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
andares.numandar AS andar, 
ambientes.descricao AS ambientes_descricao, 
setores.nomesetor AS setor,       
chamadosaux.descricao AS chamadosaux_descricao,
(select nome from funcionarios where idfuncionario = locacoes.funcionarios_idfuncionario) AS funcionario
FROM solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados, locacoes, funcionarios 
WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
AND chamadosaux.chamados_idchamado = chamados.idchamado
AND servicos.setores_idsetor = setores.idsetor
AND unidades.idunidade = andares.unidades_idunidade
AND ambientes.andares_idandar = andares.idandar
AND chamados.ambientes_idambiente = ambientes.idambiente
AND chamados.servicos_idservico = servicos.idservico
AND locacoes.chamados_idchamado = chamados.idchamado
AND locacoes.funcionarios_idfuncionario = funcionarios.idfuncionario
AND chamados.status_idstatus != (select idstatus from status where status.descricao = 'Em aberto')





