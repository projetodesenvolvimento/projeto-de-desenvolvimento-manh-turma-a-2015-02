
INSERT INTO unidades(numunidade, nomeunidade) VALUES ('63','Fatec POAI');
INSERT INTO unidades(numunidade, nomeunidade) VALUES ('65','Fatec POAII');

INSERT INTO andares(numandar, unidades_idunidade) VALUES ('1','1');
INSERT INTO andares(numandar, unidades_idunidade) VALUES ('2','1');
INSERT INTO andares(numandar, unidades_idunidade) VALUES ('3','1');
INSERT INTO andares(numandar, unidades_idunidade) VALUES ('1','2');
INSERT INTO andares(numandar, unidades_idunidade) VALUES ('2','2');
INSERT INTO andares(numandar, unidades_idunidade) VALUES ('3','2');

INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro F - Bloco A','1');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro M - Bloco A','1');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Sala 203 - Bloco A','2');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro M - Bloco A','2');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Sala 303 - Bloco A','3');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro F - Bloco A','3');

INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro F','4');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro M','4');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Sala 203','5');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro','5');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Sala 303','6');
INSERT INTO ambientes(descricao, andares_idandar) VALUES ('Banheiro','6');

INSERT INTO setores(nomesetor) VALUES ('Limpeza');
INSERT INTO setores(nomesetor) VALUES ('Manutenção');
INSERT INTO setores(nomesetor) VALUES ('TI');

INSERT INTO servicos(sla, grauseveridade, setores_idsetor, descricao) VALUES ('02:00:00','2','1','Limpeza');
INSERT INTO servicos(sla, grauseveridade, setores_idsetor, descricao) VALUES ('01:30:00','3','1','Falta Papel Higienico');
INSERT INTO servicos(sla, grauseveridade, setores_idsetor, descricao) VALUES ('03:00:00','1','2','Ar-Condicionado');
INSERT INTO servicos(sla, grauseveridade, setores_idsetor, descricao) VALUES ('05:00:00','1','2','Vazamento');
INSERT INTO servicos(sla, grauseveridade, setores_idsetor, descricao) VALUES ('06:30:00','3','3','Computador não Liga');
INSERT INTO servicos(sla, grauseveridade, setores_idsetor, descricao) VALUES ('06:30:00','3','3','Cabo de Rede Estragado');

INSERT INTO status (idstatus, descricao) VALUES (1, 'Em aberto');
INSERT INTO status (idstatus, descricao) VALUES (2, 'Em atendimento');
INSERT INTO status (idstatus, descricao) VALUES (3, 'Cancelado');
INSERT INTO status (idstatus, descricao) VALUES (4, 'Finalizado');

INSERT INTO funcionarios (senha, nome, matricula, tipo) VALUES ('123456', 'Funcionário 1', '123456', '1');
INSERT INTO funcionarios (senha, nome, matricula, tipo) VALUES ('123456', 'Funcionário 2', '123456', '2');
INSERT INTO funcionarios (senha, nome, matricula, tipo) VALUES ('123456', 'Funcionário 3', '123456', '3');

INSERT INTO solicitantes (nome, matricula, senha, email, tipo) VALUES ('admin', '9999', '9999', 'admin@gmail.com', '1');

SELECT idsolicitante, nome FROM solicitantes WHERE matricula = 9999 AND senha = 9999