
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Posso Ajudar?</title>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/nivo-lightbox.css" rel="stylesheet" />
        <link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
        <link href="css/animate.css" rel="stylesheet" />
        <!-- Squad theme CSS -->
        <link href="css/style.css" rel="stylesheet">
        <link href="color/default.css" rel="stylesheet">

    </head>

    <body data-spy="scroll">

        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="#abrir" class="gn-icon gn-icon-download">Abrir</a>
                                </li>
                                <li><a href="#visualizar" class="gn-icon gn-icon-cog">Visualizar</a></li>

                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="index.html">Posso Ajudar?</a></li>

            </ul>
        </div>

        <!-- Section: intro -->
        <section id="intro" class="intro" >
            <div class="slogan">
                <h1>Chamados</h1>

                <main>
                    <!-- Formulários -->
                    <form class="form-horizontal">

                        <table class="table table-bordered">
                            <thead style="color: white;">
                                <tr>
                                    <td>Código</td>
                                    <td>Serviço</td>
                                    <td>Status</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="success">
                                    <td>1</td>
                                    <td>Limpeza</td>
                                    <td>Aberto</td>
                                </tr>
                                <tr class="success">
                                    <td>2</td>
                                    <td>Limpeza</td>
                                    <td>Aberto</td>
                                </tr>
                                <tr class="danger">
                                    <td>3</td>
                                    <td>Manutenção</td>
                                    <td>Fechado</td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </main>
        </section>
        <!-- /Section: intro -->



        <!-- Section: works 
    <section id="works" class="home-section text-center bg-gray">
                <div class="heading-works marginbot-50">
                        <div class="container">
                        <div class="row">
                                
                        </div>
                        </div>
                </div>
                </div>
        </section>

        -->
        <!-- /Section: works -->

        <!-- Section: contact -->
        <section id="contact" class="home-section text-center">

            <div class="container">

                <div class="row">
                    <div class="col-lg-8 col-md-offset-2">

                        <div class="widget-contact row">
                            <div class="col-lg-6">
                                <address>
                                    <strong>Faculdade Senac Ltd.</strong><br>
                                    Coronel Genuino, 358 <br>
                                    Porto Alegre, RS <br>
                                    <abbr title="Phone">P:</abbr> (051) 3212-4444
                                </address>
                            </div>

                            <div class="col-lg-6">
                                <address>
                                    <strong>Email</strong><br>
                                    <a href="mailto:#">atendimentofatecpoa@senacrs.com.br</a><br />

                                </address>	

                            </div>
                        </div>	
                    </div>

                </div>	

            </div>
        </section>
        <!-- /Section: contact -->

        <footer id="footercolor">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <p>Copyright &copy; 2015 Ricardo Correa Andrade</p>
                    </div>
                    <!-- 
                        All links in the footer should remain intact. 
                        Licenseing information is available at: http://bootstraptaste.com/license/
                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Ninestars
                    -->
                </div>	
            </div>
        </footer>

        <!-- Core JavaScript Files -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.min.js"></script>	
        <script src="js/classie.js"></script>
        <script src="js/gnmenu.js"></script>
        <script src="js/jquery.scrollTo.js"></script>
        <script src="js/nivo-lightbox.min.js"></script>
        <script src="js/stellar.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="js/custom.js"></script>

    </body>

</html>

