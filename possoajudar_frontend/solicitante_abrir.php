<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Posso Ajudar? - Abrir</title>

  <!-- Bootstrap -->

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/layout.css" rel="stylesheet">
  <link href="./Slidebars_files/css" rel="stylesheet" type="text/css">
  
 
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      
      <header>

        
      <nav id="menu">
  <header>
    <h2>Menu</h2>
  </header>
</nav>

<main id="panel">
  <header>
    <h2>Panel</h2>
  </header>
</main>
    




        <h1 id="titulo">Posso Ajudar?</h1>
      </header>
      <main>
        <!-- Formulários -->
        <form class="form-horizontal">
          <!-- Campus -->
          <div class="form-group">
            <label class="col-md-2 control-label">Campus</label>
            <div class="col-md-8">
              <select class="form-control">
                <option>Selecione</option>
                <option>Campus 1</option>
                <option>Campus 2</option>
                <option></option>
              </select>
            </div>
          </div>
          
          <!-- Andar -->
          <div class="form-group">
            <label  class="col-md-2 control-label">Andar</label>
            <div class="col-md-8">

              <select class="form-control">
                <option>Selecione</option>
                <option>1º</option>
                <option>2º</option>
                <option>3º</option>
                <option>4º</option>
                <option>5º</option>
                <option>6º</option>
                <option>7º</option>
                <option>8º</option>
                <option>9º</option>
              </select>
            </div>
          </div>

          <!-- Ambiente -->
          <div class="form-group">
            <label  class="col-md-2 control-label">Ambiente</label>
            <div class="col-md-8">
              <select class="form-control">
                <option>Selecione</option>
                <option>Sala 503</option>
                <option>Sala 504</option>
                <option>Banheiro</option>
              </select>
            </div>
          </div>
          <!-- Setor -->

          <div class="form-group">
            <label  class="col-md-2 control-label">Setor</label>
            <div class="col-md-8">
              <select class="form-control">
                <option>Selecione</option>
                <option>Limpeza</option>
                <option>Manutenção</option>
                <option>TI</option>
              </select>
            </div>
          </div>
          <!-- Serviço -->
          <div class="form-group">
            <label  class="col-md-2 control-label">Serviço</label>
            <div class="col-md-8">
              <select class="form-control">
                <option>Selecione</option>
                <option>Trocar papel higiênico</option>
                <option>Troca sabonete</option>
                <option>Limpar sala</option>
              </select>
            </div>
          </div>
          <!-- Descrição -->
          <div class="form-group">
            <label  class="col-md-2 control-label">Descrição</label>
            <div class="col-md-8">
              <textarea class="form-control" maxlength="140"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
              <button type="submit" class="btn btn-primary" id="enviar_btn">ENVIAR</button>
            </div>
          </div>
        </form>
      </main>
      <footer>

      </footer>


      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slideout/0.1.9/slideout.min.js"></script>
      <script src="dist/slideout.min.js"></script>
<script>
  var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu': document.getElementById('menu'),
    'padding': 256,
    'tolerance': 70
  });

  // Toggle button
      document.querySelector('.toggle-button').addEventListener('click', function() {
        slideout.toggle();
      });
</script>


    </body>
    </html>