<?php

// declaração de classe
class ChamadoAux {
    
    // declaração de atributos
    // encapsulamento:
    // private = apenas na classe
    // protected = na classe e nas subclasses
    // public = aberta
    protected $idchamadoaux;
    protected $descricao;
    protected $horafim;
    protected $horainicio;
    protected $principal;
    protected $imagem;
    protected $chamados_idchamados;
    protected $solicitante_idsolicitante;
    
    // construtor é definido pela palavra reservada __construct
    // pode ou não ter parâmetros
    // para um construtor poder não receber parâmetros deve-se
    // definir valores padrão como neste exemplo
    
    function __construct($idchamadoaux, $descricao, $horafim, $horainicio, $principal, $imagem, $chamados_idchamado, $solicitantes_idsolicitante) {
        $this->idchamadoaux = $idchamadoaux;
        $this->descricao = $descricao;
        $this->horafim = $horafim;
        $this->horainicio = $horainicio;
        $this->principal = $principal;
        $this->imagem = $imagem;
        $this->chamados_idchamado = $chamados_idchamado;
        $this->solicitantes_idsolicitante = $solicitantes_idsolicitante;
    }
    
    // "método mágico" para criação de set genérico
    // ou seja, cria um set que pode ser usado por todos os atributos
    function &__set($prop, $val) {
        $this->$prop = $val;
    }

    // "método mágico" para criação de get genérico
    // ou seja, cria um get que pode ser usado por todos os atributos
    function &__get($prop) {
        return $this->$prop;
    }
    
    // método para impressão de dados do objeto
    function __toString() {
        return "Esse Chamado Auxiliar  [" . $this->idchamadosaux. "] foi aberto pelo solicitante: " .$this->solicitantes_idsolicitante. ""
                . " que é referente a solicitação " . $this->chamados_idchamados;
    }
}
?>
