<?php

class Funcionario {
    protected $idfuncionario;
    protected $nome;
    protected $matricula;
    protected $senha;
    protected $tipo;

    function __construct($idfuncionario = "", $nome = "", $matricula = "", $senha = "", $tipo = "") {
        $this->idfuncionario = $idfuncionario;
        $this->nome = $nome;
        $this->matricula = $matricula;
        $this->senha = $senha;
        $this->tipo = $tipo;
    }

    // "método mágico" para criação de set genérico

    // ou seja, cria um set que pode ser usado por todos os atributos

   function &__set($prop, $val) {

        $this->$prop = $val;

   }

 // "método mágico" para criação de get genérico

    // ou seja, cria um get que pode ser usado por todos os atributos

    function &__get($prop) {

        return $this->$prop;

    }

    // método para impressão de dados do objeto

    function __toString() {

        return "O Funcionario de id [" . $this->idfuncionario . "] possui o nome de " .

                $this->nome . " , possui a matricula " . $this->matricula . "e o tipo de " . $this->tipo;
    }
}