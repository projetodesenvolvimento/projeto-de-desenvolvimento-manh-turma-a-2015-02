﻿<?php


class Servicos {
   
    protected $idservico;
    protected $sla;
    protected $grauseveridade;
    protected $descricao;
    protected $setores_idsetor;
    
    // testar commit pelo NetBeans
    
  
    function __construct($idservico = "", $sla = "", $grauseveridade = "", $descricao = "", $setores_idsetor = "") {
     
        $this->idservico = $idservico;
        $this->sla = $sla;
        $this->grauseveridade = $grauseveridade;
        $this->descricao = $descricao;
        $this->setores_idsetor = $setores_idsetor;
    }

   
    function &__set($prop, $val) {
        $this->$prop = $val;
    }


    function &__get($prop) {
        return $this->$prop;
    }

   
    function __toString() {
        return "Id [" . $this->idservico . "] - SLA " . $this->sla . ""
                . " - Grau de severidade " . $this->grauseveridade . ""
                . " - Descrição " . $this->descricao . " - IdSetor " .
                $this->setores_idsetor;
    }

}
?>