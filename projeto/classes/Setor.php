<?php

// declaração de classe
class Setor {

    // declaração de atributos
    // encapsulamento:
    // private = apenas na classe
    // protected = na classe e nas subclasses
    // public = aberta
    protected $idsetor;
    protected $nomesetor;
    
    // testar commit pelo NetBeans sem ADD e com comentário!
    

    // construtor é definido pela palavra reservada __construct
    // pode ou não ter parâmetros
    // para um construtor poder não receber parâmetros deve-se
    // definir valores padrão como neste exemplo
    function __construct($idsetor = "", $nomesetor = "") {
        // associação de valores
        // usa-se o -> para unir o objeto e o atributo
        $this->idsetor = $idsetor;
        $this->nomesetor = $nomesetor;
                
    }

    // "método mágico" para criação de set genérico
    // ou seja, cria um set que pode ser usado por todos os atributos
    function &__set($prop, $val) {
        $this->$prop = $val;
    }

    // "método mágico" para criação de get genérico
    // ou seja, cria um get que pode ser usado por todos os atributos
    function &__get($prop) {
        return $this->$prop;
    }

    // método para impressão de dados do objeto
    function __toString() {
        return "O Setor de id [".$this->idsetor."] possui o nome de ".
                $this->nomesetor;
    }

}
	

?>