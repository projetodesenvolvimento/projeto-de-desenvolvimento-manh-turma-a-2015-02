﻿<?php

// declaração de classe
class Andar {

    // declaração de atributos
    // encapsulamento:
    // private = apenas na classe
    // protected = na classe e nas subclasses
    // public = aberta
    protected $idandar;
    protected $numandar;
    protected $unidades_idunidade;
    
    // testar commit pelo NetBeans

    // construtor é definido pela palavra reservada __construct
    // pode ou não ter parâmetros
    // para um construtor poder não receber parâmetros deve-se
    // definir valores padrão como neste exemplo
    function __construct($idunidade = "", $numandar = "", $unidades_idunidade = "") {
        // associação de valores
        // usa-se o -> para unir o objeto e o atributo
        $this->idunidade = $idunidade;
        $this->numandar = $numandar;
        $this->unidades_idunidade = $unidades_idunidade;
        
    }

    // "método mágico" para criação de set genérico
    // ou seja, cria um set que pode ser usado por todos os atributos
    function &__set($prop, $val) {
        $this->$prop = $val;
    }

    // "método mágico" para criação de get genérico
    // ou seja, cria um get que pode ser usado por todos os atributos
    function &__get($prop) {
        return $this->$prop;
    }

    // método para impressão de dados do objeto
    function __toString() {
        return "O andar de id  [" . $this->idandar . "] e número " .$this->numandar . ""
                . " pertence à unidade de ID " . $this->unidades_idunidade;
    }

}
?>