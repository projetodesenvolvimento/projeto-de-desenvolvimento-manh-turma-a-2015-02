<?php

/**
 * Description of Chamado
 *
 * @author vitor
 */
class Chamado {

    protected $idchamado;
    protected $status_idstatus;
    protected $servicos_idservico;
    protected $ambientes_idambiente;

    function __construct($idchamado = "", $status_idstatus = "", $servicos_idservico = "", $ambientes_idambiente = "") {
        // associação de valores
        // usa-se o -> para unir o objeto e o atributo
        $this->idchamado = $idchamado;
        $this->status_idstatus = $status_idstatus;
        $this->servicos_idservico = $servicos_idservico;
        $this->ambientes_idambiente = $ambientes_idambiente;
    }

    // "método mágico" para criação de set genérico
    // ou seja, cria um set que pode ser usado por todos os atributos
    function &__set($prop, $val) {
        $this->$prop = $val;
    }

    // "método mágico" para criação de get genérico
    // ou seja, cria um get que pode ser usado por todos os atributos
    function &__get($prop) {
        return $this->$prop;
    }

    // método para impressão de dados do objeto
    function __toString() {
        return "O Chamado de id [" . $this->idchamado . "] possui o status de " .
                $this->status_idstatus . " , possui o serviço de id " . $this->servicos_idservico . "e o ambiente de id" . $thiis->ambientes_idambiente;
    }

}

?>
