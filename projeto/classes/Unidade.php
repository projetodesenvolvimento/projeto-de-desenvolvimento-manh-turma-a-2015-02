﻿<?php

// declaração de classe
class Unidade {

    // declaração de atributos
    // encapsulamento:
    // private = apenas na classe
    // protected = na classe e nas subclasses
    // public = aberta
    protected $idunidade;
    protected $numunidade;
    protected $nomeunidade;
    
     // testar commit pelo NetBeans

    // construtor é definido pela palavra reservada __construct
    // pode ou não ter parâmetros
    // para um construtor poder não receber parâmetros deve-se
    // definir valores padrão como neste exemplo
    function __construct($idunidade = "", $numunidade = "", $nomeunidade = "") {
        // associação de valores
        // usa-se o -> para unir o objeto e o atributo
        $this->idunidade = $idunidade;
        $this->numunidade = $numunidade;
        $this->nomeunidade = $nomeunidade;
                
    }

    // "método mágico" para criação de set genérico
    // ou seja, cria um set que pode ser usado por todos os atributos
    function &__set($prop, $val) {
        $this->$prop = $val;
    }

    // "método mágico" para criação de get genérico
    // ou seja, cria um get que pode ser usado por todos os atributos
    function &__get($prop) {
        return $this->$prop;
    }

    // método para impressão de dados do objeto
    function __toString() {
        return "A unidade de id [".$this->idunidade."] tem o número de ".
                $this->numunidade. " e o nome de: ".$this->nomeunidade;
    }

}
	

?>