<?php


class Alocacao {
    //put your code here
    protected $idchamado;
    protected $idfuncionario;
    
    function __construct($idchamado, $idfuncionario) {
        $this->idchamado = $idchamado;
        $this->idfuncionario = $idfuncionario;
    }
    
    function &__set($prop, $val) {
        $this->$prop = $val;
    }
    
    function &__get($prop) {
        return $this->$prop;
    }
    
    function __toString() {
        return "Esse Chamado [" . $this->idchamado. "] foi alocado para o funcionário: " .$this->idfuncionario;
    }
}
