﻿<?php

// declaração de classe
class Ambiente {

    // declaração de atributos
    // encapsulamento:
    // private = apenas na classe
    // protected = na classe e nas subclasses
    // public = aberta
    protected $idambiente;
    protected $descricao;
    protected $andares_idandar;
    

    // testar commit pelo NetBeans sem ADD
      
    // construtor é definido pela palavra reservada __construct
    // pode ou não ter parâmetros
    // para um construtor poder não receber parâmetros deve-se
    // definir valores padrão como neste exemplo
    function __construct($idambiente = "", $descricao = "", $andares_idandar = "") {
        // associação de valores
        // usa-se o -> para unir o objeto e o atributo
        $this->idambiente = $idambiente;
        $this->descricao = $descricao;
        $this->andares_idandar = $andares_idandar;
        
    }

    // "método mágico" para criação de set genérico
    // ou seja, cria um set que pode ser usado por todos os atributos
    function &__set($prop, $val) {
        $this->$prop = $val;
    }

    // "método mágico" para criação de get genérico
    // ou seja, cria um get que pode ser usado por todos os atributos
    function &__get($prop) {
        return $this->$prop;
    }

    // método para impressão de dados do objeto
    function __toString() {
        return "O Ambiente de id [".$this->idambiente."] possui a descrição de ".
                $this->descricao." e localiza-se no andar de id ".$this->idandar; 
    }

}
	

?>