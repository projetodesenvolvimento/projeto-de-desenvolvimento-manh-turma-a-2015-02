<?php

class Solicitante {
    protected $idsolicitante;
    protected $nome;
    protected $matricula;
    protected $senha;
    protected $email;
    protected $tipo;

        
    function __construct($idsolicitante = "", $nome = "", $matricula = "", $senha = "", $email = "", $tipo = "") {


        $this->idsolicitante = $idsolicitante;
        $this->nome = $nome;
        $this->matricula = $matricula;
	$this->senha = $senha;
	$this->email = $email;
	$this->tipo = $tipo;
    }

    // "método mágico" para criação de set genérico

    // ou seja, cria um set que pode ser usado por todos os atributos

   function &__set($prop, $val) {

        $this->$prop = $val;

   }

 // "método mágico" para criação de get genérico

    // ou seja, cria um get que pode ser usado por todos os atributos

    function &__get($prop) {

        return $this->$prop;

    }

    // método para impressão de dados do objeto

    function __toString() {

        return "O Solicitante de id [" . $this->idsolicitante . "] possui o nome de " .

                $this->nome . " , possui a matricula " . $this->matricula . "e o tipo de " . $this->tipo;

    }
}
