<?php
require 'includes/validacao.php';
require "dao/UnidadeDAO.php";
require "dao/AmbienteDAO.php";
require "dao/AndarDAO.php";
require "dao/SetorDAO.php";
require "dao/ServicoDAO.php";

// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "cabecalho.php";

?>

<main>
    <h1>Abrir solicitação</h1>
    
    <!-- Formulários -->
    <form class="form-horizontal" action="cadastrarchamado.php" method="post">
        <!-- Campus -->
        <div id="formcolor">
            <div class="form-group">
                <label class="col-md-2 control-label">Campus</label>
                <div class="col-md-8">

                    <select name="unidade" class="form-control">
                        <?php
                        $unidade = new UnidadeDAO();
                        $unidade->visualizar("", "true");
                        ?>
                    </select>

                </div>
            </div>

            <!-- Andar -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Andar</label>
                <div class="col-md-8">
                    <select name="andar" class="form-control"  disabled="true">
                        <option value='0'>Selecione um Andar</option>
                    </select>
                </div>
            </div>

            <!-- Ambiente -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Ambiente</label>
                <div class="col-md-8">
                    <select name="ambiente" class="form-control" disabled="true">
                        <option value='0'>Selecione um Ambiente</option>
                    </select>
                </div>
            </div>
            <!-- Setor -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Setor</label>
                <div class="col-md-8">
                    <select name="setor" class="form-control" disabled="true">
                        <?php
                        $setor = new SetorDAO();
                        $setor->visualizar("", "true");
                        ?>
                    </select>
                </div>
            </div>
            <!-- Serviço -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Serviço</label>
                <div class="col-md-8">
                    <select name="servico" class="form-control" disabled="true">
                        <option value='0'>Selecione um Serviço</option>
                    </select>
                </div>
            </div>
            <!-- Descrição -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Descrição</label>
                <div class="col-md-8">
                    <textarea name="descricao" class="form-control" maxlength="120" disabled="true"></textarea>
                </div>

            </div>

            <!-- Imagem -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Adicionar imagem</label>
                <div class="col-md-8">
                    <input type="file" name="imagem" class="form-control" disabled="true">
                </div>
            </div>

        </div>
        <button type="submit" class="form-group btn btn-warning" disabled="true">ENVIAR</button>
    </form>
    
</main>

<?php        
// inclusão do rodapé com toda fechamento da estrutura HTML e chamadas de JS
include "rodape.php";
?>

<script type="text/javascript">
       
      $(document).ready(function(){
          
        $("select[name=unidade]").change(function() {
            $("select[name=andar]").html('<option value="0">Carregando...</option>');
            $.post("multicombo.php?op=andar", {unidade:$(this).val()}, function(valor){ $("select[name=andar]").html(valor); });
            $("select[name=andar]").prop( "disabled", false );
        });
        
        $("select[name=andar]").change(function() {
            $("select[name=ambiente]").html('<option value="0">Carregando...</option>');
            $.post("multicombo.php?op=ambiente", {andar:$(this).val()}, function(valor){ $("select[name=ambiente]").html(valor); });
            $("select[name=ambiente]").prop( "disabled", false );
        });
        
        $("select[name=ambiente]").change(function() {
           $("select[name=setor]").prop( "disabled", false );
        });
        
        $("select[name=setor]").change(function() {
            $("select[name=servico]").html('<option value="0">Carregando...</option>');
            $.post("multicombo.php?op=servico", {setor:$(this).val()}, function(valor){ $("select[name=servico]").html(valor); });
            $("select[name=servico]").prop( "disabled", false );    
            $("textarea[name=descricao]").prop( "disabled", false );
            $("input[type=file]").prop( "disabled", false );
            $("button[type=submit]").prop( "disabled", false );
        });
         
      });
       
</script>

