<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "cabecalho.php";
?>
            <main>
                <!-- Formulários -->
                <form class="form-horizontal" action="autenticar.php" method="post">
                    <!-- Campus -->
                    <div id="formcolor">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Matrícula</label>
                            <div class="col-md-8">
                                <input type="text" name="matricula" id="matricula" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-md-2 control-label">Senha</label>
                            <div class="col-md-8">
                                <input type="password" name="senha" id="senha" class="form-control" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="op" value="<?php if(isset($_GET['op'])) echo 'admin'; ?>">
                    <input type="submit" name="acao" class="form-group btn btn-warning" value="LOGAR">
                </form>
            </main>

<?php        
// inclusão do rodapé com toda fechamento da estrutura HTML e chamadas de JS
include "rodape.php";
?>
