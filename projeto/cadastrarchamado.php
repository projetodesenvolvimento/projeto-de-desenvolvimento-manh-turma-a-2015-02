﻿<?php
require 'includes/validacao.php';

// verifica se as informações vieram 
// através do método POST
if (isset($_POST)) {
    include "classes/Chamado.php";
    include "classes/ChamadoAux.php";
    include "dao/ChamadoDAO.php";
    include "dao/ChamadoAuxDAO.php";

    // recebe os valores vindos do formulário através de post
    $unidade = $_POST["unidade"];
    $andar = $_POST["andar"];
    $ambiente = $_POST["ambiente"];
    $setor = $_POST["setor"];
    $servico = $_POST["servico"];
    $descricao = $_POST["descricao"];
    
    // ATENCAO: adicionar campo de upload de imagem no front-end + o esquema upload
    //$imagem = "";
    
    // BUSCA ID DA SESSÃO
    $idsolicitante = $_SESSION["idsolicitante"];

    $chamado = new Chamado("", "1", $servico, $ambiente);
    $acoes = new ChamadoDAO();
    $idchamado = $acoes->inserir($chamado);
    
    // ATENÇÃO: realizar regra de negócio para verificar se é o primeiro chamado ou não...
    $principal = 1;
    
    date_default_timezone_set("America/Sao_Paulo");
    $horainicio = date("Y-m-d H:i:s");
    $horafim = "";
    
    $chamadoaux = new ChamadoAux("", $descricao, $horafim, $horainicio, $principal, $imagem, $idchamado, $idsolicitante);
    $acoes = new ChamadoAuxDAO();
    $acoes->inserir($chamadoaux);
}
?>

<script type="text/javascript">location.href="solicitante_chamados.php";</script>