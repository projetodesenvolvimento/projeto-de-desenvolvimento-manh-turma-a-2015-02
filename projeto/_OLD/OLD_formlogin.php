<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Autenticação</title>	

    </head>
    <body>
        <!-- Lembrar sempre de enviar o formulário de autenticação por
             "post" para aumentar a segurança do processo -->
        <fieldset>
            <legend>Dados do Usuário</legend>
            <form action="autenticar.php" method="post">
                <label for="matricula">Matrícula</label>
                <input name="matricula" id="matricula" type="text"/>
                <label for="senha">Senha</label>
                <input name="senha" id="senha" type="password"/>
                <input name="acao" type="submit" value="Enviar" />
            </form>
        </fieldset>
    </body>
</html>
