<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Ambientes</title>	
        <link href="css/geral.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <h1>Página de Teste da Entidade - Visualizar e Inserir</h1>
        

        <h2>Inserir</h2>
        <form name="form1" action="cadastrarambientes.php" method="post">
            <fieldset>
                <legend>Dados do Ambiente - Informe a descrição do ambiente e o id do andar desejado (número entre 1 e 6)</legend>
                <input type="text" name="descricao" id="nome" placeholder="Descrição: ">
                <input type="text" name="idandar" id="idandar" placeholder="idandar: ">
                <input type="submit" name="acao" value="Enviar" class="botao"/>
            </fieldset>	
        </form>

        <?php
        include "dao/AmbienteDAO.php";

        // instancia um objeto da classe UsuarioDadosDAO para dar acesso aos métodos do CRUD
        $acoes = new AmbienteDAO();

        // chama o método de de visualização de dados
        // neste caso apresenta todos os registros que estão na tabela Usuarios
        echo "<h1>Visualizar todos os registros da entidade Ambientes</h1>";
        $acoes->visualizar();

        // chama o método de de visualização de dados passando o parâmetro de id
        // neste caso apresenta os dados do registro que tem id 1 na tabela Usuarios
        echo "<h1>Visualizar apenas registro específico</h1>";
        $acoes->visualizar(1);
        ?>

    </body>
</html>