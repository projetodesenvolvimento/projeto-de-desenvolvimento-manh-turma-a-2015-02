<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Ambientes</title>	
        <link href="css/geral.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <h1>Página de Teste da Entidade - Listagem</h1>
        

        <h2>Lista</h2>

        <?php
        require 'includes/validacao.php';
        
        include 'dao/ChamadoAuxDAO.php';

        $id = $_REQUEST["id"];
        
        $mostrar = new ChamadoAuxDAO();

        echo "<h1>Visualizar apenas registro específico</h1>";
        echo $mostrar->listarChamadoDetalhe($id);
        ?>

    </body>
</html>