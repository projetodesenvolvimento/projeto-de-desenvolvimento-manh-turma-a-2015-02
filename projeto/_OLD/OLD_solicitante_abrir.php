<?php
    require 'includes/validacao.php';
    
    echo $_SESSION["idsolicitante"]." - ".$_SESSION["nome"];
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Posso Ajudar? - Abrir</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/layout.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
    </head>

    <body>      
        <header>

            <div class="sidebar-wrapper sidebar-default">
                <div class="sidebar-scroller">
                    <ul class="sidebar-menu">
                        <li class="sidebar-group">
                            <span>Header 1</span>
                            <ul class="sidebar-group-menu">
                                <li class="sidebar-item"><a href="#">Item 1</a></li>
                                <li class="sidebar-item"><a href="#" class="active">Item 2</a></li>
                                <li class="sidebar-item"><a href="#">Item 3</a></li>
                            </ul>
                        </li>
                        <li class="sidebar-group">
                            <span>Header 2</span>
                            <ul class="sidebar-group-menu">
                                <li class="sidebar-item"><a href="#">Item 4</a></li>
                                <li class="sidebar-item"><a href="#">Item 5</a></li>
                                <li class="sidebar-item"><a href="#">Item 6</a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"><a href="#">Item 7</a></li>
                        <li class="sidebar-item"><a href="#">Item 8</a></li>
                        <li class="sidebar-item"><a href="#">Item 9</a></li>
                    </ul>
                </div>
            </div>



            <!-- Metodos do PHP -->

            <?php
            require "dao/UnidadeDAO.php";
            require "dao/AmbienteDAO.php";
            require "dao/AndarDAO.php";
            require "dao/SetorDAO.php";
            require "dao/ServicoDAO.php";
            ?>

            <h1 id="titulo">Posso Ajudar?</h1>
        </header>
        <main>
            <!-- Formulários -->
            <form class="form-horizontal" action="cadastrarchamado.php" method="post">
                <!-- Campus -->
                <div class="form-group">
                    <label class="col-md-2 control-label">Campus</label>
                    <div class="col-md-8">
                        <select name="unidade" class="form-control">
                            <?php
                            $unidade = new UnidadeDAO();
                            $unidade->visualizar("", "true");
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Andar -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Andar</label>
                    <div class="col-md-8">

                        <select name="andar" class="form-control">
                            <?php
                            $andar = new AndarDAO();
                            $andar->visualizar("", "true");
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Ambiente -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Ambiente</label>
                    <div class="col-md-8">
                        <select name="ambiente" class="form-control">
                            <?php
                            $ambiente = new AmbienteDAO();
                            $ambiente->visualizar("", "true");
                            ?>
                        </select>
                    </div>
                </div>

                <!-- Setor -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Setor</label>
                    <div class="col-md-8">
                        <select name="setor" class="form-control">
                            <?php
                            $setor = new SetorDAO();
                            $setor->visualizar("", "true");
                            ?>
                        </select>
                    </div>
                </div>
                <!-- Serviço -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Serviço</label>
                    <div class="col-md-8">
                        <select name="servico" class="form-control">
                            <?php
                            $servicos = new ServicoDAO();
                            $servicos->visualizar("", "true");
                            ?>
                        </select>
                    </div>
                </div>
                <!-- Descrição -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Descrição</label>
                    <div class="col-md-8">
                        <textarea name="descricao" class="form-control" maxlength="140"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <input type="submit" class="btn btn-primary" id="enviar_btn" value="ENVIAR">
                    </div>
                </div>
            </form>
        </main>
        <footer>

        </footer>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>