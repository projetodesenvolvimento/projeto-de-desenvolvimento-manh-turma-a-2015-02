<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

// estabelecimento de herança
class UnidadeDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    // método de inserção de dados na base de dados
    // recebimento de objeto com atributos setados
    public function inserir($unidade) {

        try { 

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO unidades (idunidade, numunidade, nomeunidade) VALUES (?, ?, ?)");
                        
            echo $unidade->nomeunidade;

            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $unidade->idunidade);
            $stm->bindValue(2, $unidade->numunidade);
            $stm->bindValue(3, $unidade->nomeunidade);
            
                       
            // realiza a execução do código na base
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ./index.php");
            }

            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

    // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    
    public function visualizar($idunidade = "", $combo = "") {

        try { 

            if($idunidade == "") {
                $stm = $this->conexao->prepare("SELECT * FROM unidades");
                
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM unidades WHERE idunidade = ?");
                $stm->bindParam(1, $idunidade, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                if($combo == ""){
                    
                
                // Para cada resultado encontrado...
                $tabela = "<table><tr>"
                        ."<td>IDUNIDADE</td>"
                        ."<td>NÚMERO DA UNIDADE</td>"
                        ."<td>NOME DA UNIDADE</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idunidade."</td>"
                             ."<td>".$dados->numunidade."</td>"
                             ."<td>".$dados->nomeunidade."</td>"
                             ."</tr>"; 
             
                }
                $tabela .= "</table>";
                    
                    echo $tabela;
                
                    
                }else {
                    
                    $ops = "<option value='0'>Selecione um Campus</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idunidade."'>".$dados->numunidade." - ".$dados->nomeunidade."</option>";
                    }
                    echo $ops;
                }
            }

        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }
       
}

?>