<?php
// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

/**
 * Description of ChamadoDAO
 *
 * @author vitor
 */
class ChamadoDAO extends BancoPDO {
    
    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }
    
     // método de inserção de dados na base de dados
    // recebimento de objeto com atributos setados
    public function inserir($chamado) {

        try { 

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO chamados (status_idstatus, servicos_idservico, ambientes_idambiente)"
                    . " VALUES (?, ?, ?)");
            
            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $chamado->status_idstatus);
            $stm->bindValue(2, $chamado->servicos_idservico);
            $stm->bindValue(3, $chamado->ambientes_idambiente);
             
            // realiza a execução do código na base
            if($stm->execute()) {
                $chamados_idchamado = $this->conexao->lastInsertId(); 
                return $chamados_idchamado;
            }
            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }
    
 // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    public function visualizar($idchamado = "") {

        try { 

            if($idchamado == "") {
                $stm = $this->conexao->prepare("select projeto.chamados.idchamado, projeto.servicos.descricao,projeto.status.descricao,projeto.ambientes.descricao from projeto.chamados,projeto.servicos,projeto.status,projeto.ambientes where projeto.servicos.idservico = projeto.chamados.idchamado and projeto.status.idstatus = projeto.chamados.idchamado and projeto.ambientes.idambiente = projeto.chamados.idchamado");
            } else {
                $stm = $this->conexao->prepare("select projeto.chamados.idchamado, projeto.servicos.descricao,projeto.status.descricao,projeto.ambientes.descricao from projeto.chamados,projeto.servicos,projeto.status,projeto.ambientes where projeto.servicos.idservico = ? and projeto.status.idstatus = ? and projeto.ambientes.idambiente = ?");
                $stm->bindParam(1, $idchamado, PDO::PARAM_INT);
                 $stm->bindParam(2, $idchamado, PDO::PARAM_INT);
                  $stm->bindParam(3, $idchamado, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                
                // Para cada resultado encontrado...
                $tabela = "<table><tr>"
                        ."<td>IDCHAMADO</td>"
                        ."<td>SERVIÇO</td>"
                        ."<td>STATUS</td>"
                        ."<td>LOCAL</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idchamado."</td>"
                             ."<td>".$dados->status_idstatus."</td>"
                             ."<td>".$dados->servicos_idservico."</td>"
                             ."<td>".$dados->ambientes_idambiente."</td>"
                             ."</tr>"; 
                }
                
                $tabela .= "</table>";
            
                echo $tabela;
                
            }

        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }   
    
    
    public function trocaStatus($idchamado="", $idstatus="") {
        
        $stm = $this->conexao->prepare("UPDATE chamados SET status_idstatus = ? WHERE idchamado = ?");
        $stm->bindParam(1, $idstatus, PDO::PARAM_INT);
        $stm->bindParam(2, $idchamado, PDO::PARAM_INT);
        
        if($stm->execute())
            return true;
        else 
            return false;
    }
    
}

?>