<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");


class ServicoDAO extends BancoPDO {

   
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

   
    public function inserir($servico) {

        try { 

            $stm = $this->conexao->prepare("INSERT INTO servicos (idservico, sla, grauseveridade, descricao, setores_idsetor) VALUES (?, ?, ?, ?, ?)");
                        
            echo $servico->descricao;

        
            $stm->bindValue(1, $servico->idservico);
            $stm->bindValue(2, $servico->sla);
            $stm->bindValue(3, $servico->grauseveridade);
            $stm->bindValue(4, $servico->descricao);
            $stm->bindValue(5, $servico->setores_idsetor);
            
                       
           
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ./index.php");
            }

            
        
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

 
    public function visualizar($idservico = "", $combo = "", $filtro = "") {

        try { 
             
            if($filtro != "") {
                $stm = $this->conexao->prepare("SELECT * FROM servicos WHERE setores_idsetor = ?");
                $stm->bindParam(1, $filtro, PDO::PARAM_INT);  
            } else if($idservico == "") {
                $stm = $this->conexao->prepare("SELECT * FROM servicos");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM servicos WHERE idservico = ?");
                $stm->bindParam(1, $idservico, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                if($combo ==""){
                $tabela = "<table><tr>"
                        ."<td>IDSERVICOS</td>"
                        ."<td>SLA</td>"
                        ."<td>GRAU DE SEVERIDADE</td>"
                        ."<td>DESCRIÇÃO</td>"
                        ."<td>IDSETOR</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idservico."</td>"
                             ."<td>".$dados->sla."</td>"
                             ."<td>".$dados->grauseveridade."</td>"
                             ."<td>".$dados->descricao."</td>"
                             ."<td>".$dados->setores_idsetor."</td>"  
                             ."</tr>"; 
                }
            
                $tabela .= "</table>";
            
                echo $tabela;
            }else{
                
                $ops = "<option value='0'>Selecione um Serviço</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idservico."'>".utf8_encode($dados->descricao)."</option>";
                    }
                    echo $ops;
                
            }
        }
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }
    
   
}

?>