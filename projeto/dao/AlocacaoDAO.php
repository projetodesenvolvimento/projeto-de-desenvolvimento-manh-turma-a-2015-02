<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");


class AlocacaoDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    public function inserir($locacoes) {

        try {

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO locacoes(funcionarios_idfuncionario, chamados_idchamado) VALUES (?, ?)");
                    
            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $locacoes->idfuncionario);
            $stm->bindValue(2, $locacoes->idchamado);           
            
            // realiza a execução do código na base
            if($stm->execute())
                return true;
            else
                return false;
                //echo "Dados inseridos com sucesso! <br/>";
                //header("Location: ../formLocacao.php");
           

            // tratamento de exceção nativo de PDO
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    public function visualizar($chamadoAux = "", $combo = "") {

        try {

            if ($chamadoAux == "") {
                $stm = $this->conexao->prepare("SELECT * FROM locacoes");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM locacoes WHERE chamados_idchamado = ?");
                $stm->bindParam(1, $chamado, PDO::PARAM_INT);
            }

            if ($stm->execute()) {
                if ($combo == "") {

                    while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                        $dados->chamados_idchamado;
                        $dados->funcionarios_idfuncionario;
                    }
                }
            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    public function listarLocacoesAbertas() {

        try {

                $stm = $this->conexao->prepare("SELECT chamadosaux.chamados_idchamado AS chamado,
                                                        servicos.descricao AS servicos_descricao,
                                                        (select descricao from status where idstatus = chamados.status_idstatus) AS status,
                                                        solicitantes.matricula AS matricula, 
                                                        (select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
                                                        andares.numandar AS andar, 
                                                        ambientes.descricao AS ambientes_descricao, 
                                                        setores.nomesetor AS setor,       
                                                        chamadosaux.descricao AS chamadosaux_descricao       
                                                        FROM solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados 
                                                        WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
                                                        AND chamadosaux.chamados_idchamado = chamados.idchamado
                                                        AND servicos.setores_idsetor = setores.idsetor
                                                        AND unidades.idunidade = andares.unidades_idunidade
                                                        AND ambientes.andares_idandar = andares.idandar
                                                        AND chamados.ambientes_idambiente = ambientes.idambiente
                                                        AND chamados.servicos_idservico = servicos.idservico
                                                        AND chamados.status_idstatus = (select idstatus from status where status.descricao = ?)");
                
                $stm->bindValue(1, "Em aberto");

            if ($stm->execute()) {
                
                 while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                     
                    if($dados->status == "Em aberto")
                        $sts = "danger";
                    else if ($dados->status == "Em atendimento")
                        $sts = "active";
                    else 
                        $sts = "success";
                     
                    echo "<form action='../admin/cadastros/cadastrarlocacao.php' method='post'>";
                    echo "<tr class='".$sts."'>"
                            . "<td>".$dados->chamado."</td>"
                            . "<td> <a href='../solicitante_visualizar_chamado.php?id=".$dados->chamado."'>" . utf8_encode($dados->servicos_descricao) . "</a></td>"
                            . "<td><select name='idfuncionario' class='form-control'>";
                    
                    // impressão das opções do combo
                    $nome = new funcionarioDAO();
                    $nome->visualizar("", "true");
                    
                    echo "</select></td>"
                       . "<td> "
                            . "<input type='hidden' name='idchamado' value='".$dados->chamado."'>"
                            . "<input type='submit' class='form-group btn btn-warning' value='ALOCAR'>"
                            . "</td>"
                       . "</tr>"
                       . "</form>";    
                    
                }

            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    public function listarLocacoesAndamento() {

        try {

                $stm = $this->conexao->prepare("SELECT chamadosaux.chamados_idchamado AS chamado,
                                                        servicos.descricao AS servicos_descricao,
                                                        (select descricao from status where idstatus = chamados.status_idstatus) AS status,
                                                        solicitantes.matricula AS matricula, 
                                                        (select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
                                                        andares.numandar AS andar, 
                                                        ambientes.descricao AS ambientes_descricao, 
                                                        setores.nomesetor AS setor,       
                                                        chamadosaux.descricao AS chamadosaux_descricao,
                                                        (select nome from funcionarios where idfuncionario = locacoes.funcionarios_idfuncionario) AS funcionario
                                                        FROM solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados, locacoes, funcionarios 
                                                        WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
                                                        AND chamadosaux.chamados_idchamado = chamados.idchamado
                                                        AND servicos.setores_idsetor = setores.idsetor
                                                        AND unidades.idunidade = andares.unidades_idunidade
                                                        AND ambientes.andares_idandar = andares.idandar
                                                        AND chamados.ambientes_idambiente = ambientes.idambiente
                                                        AND chamados.servicos_idservico = servicos.idservico
                                                        AND locacoes.chamados_idchamado = chamados.idchamado
                                                        AND locacoes.funcionarios_idfuncionario = funcionarios.idfuncionario
                                                        AND chamados.status_idstatus = (select idstatus from status where status.descricao = ?)");
                
                $stm->bindValue(1, "Em atendimento");
                

            if ($stm->execute()) {
                
                 while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                     
                    if($dados->status == "Em aberto")
                        $sts = "normal";
                    else if ($dados->status == "Em atendimento")
                        $sts = "danger";
                    else 
                        $sts = "success";
                    
                    echo "<form action='../admin/cadastros/finalizarChamado.php' method='post'>";
                    echo "<tr class='".$sts."'>"
                            . "<td>".$dados->chamado."</td>"
                            . "<td><a href='../solicitante_visualizar_chamado.php?id=".$dados->chamado."'>" . utf8_encode($dados->servicos_descricao) . "</a></td>"
                            . "<td>" . utf8_encode($dados->funcionario) . "</td>"
                            . "<td> "
                                . "<input type='hidden' name='idchamado' value='".$dados->chamado."'>"
                                . "<input type='submit' class='form-group btn btn-warning' value='FINALIZAR'>"
                            . "</td>";
                    
                    echo "</select></td></form>";    
                    
                }

            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    
    public function listarLocacoesFechadas() {

        try {

                $stm = $this->conexao->prepare("SELECT chamadosaux.chamados_idchamado AS chamado,
                                                        servicos.descricao AS servicos_descricao,
                                                        (select descricao from status where idstatus = chamados.status_idstatus) AS status,
                                                        solicitantes.matricula AS matricula, 
                                                        (select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
                                                        andares.numandar AS andar, 
                                                        ambientes.descricao AS ambientes_descricao, 
                                                        setores.nomesetor AS setor,       
                                                        chamadosaux.descricao AS chamadosaux_descricao,
                                                        (select nome from funcionarios where idfuncionario = locacoes.funcionarios_idfuncionario) AS funcionario
                                                        FROM solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados, locacoes, funcionarios 
                                                        WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
                                                        AND chamadosaux.chamados_idchamado = chamados.idchamado
                                                        AND servicos.setores_idsetor = setores.idsetor
                                                        AND unidades.idunidade = andares.unidades_idunidade
                                                        AND ambientes.andares_idandar = andares.idandar
                                                        AND chamados.ambientes_idambiente = ambientes.idambiente
                                                        AND chamados.servicos_idservico = servicos.idservico
                                                        AND locacoes.chamados_idchamado = chamados.idchamado
                                                        AND locacoes.funcionarios_idfuncionario = funcionarios.idfuncionario
                                                        AND chamados.status_idstatus != (select idstatus from status where status.descricao = ?)
                                                        AND chamados.status_idstatus != (select idstatus from status where status.descricao = ?)");
                
                $stm->bindValue(1, "Em aberto");
                $stm->bindValue(2, "Em atendimento");
                

            if ($stm->execute()) {
                
                 while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                     
                    if($dados->status == "Em aberto")
                        $sts = "normal";
                    else if ($dados->status == "Em atendimento")
                        $sts = "danger";
                    else 
                        $sts = "success";
                    
                    echo "<tr class='".$sts."'>"
                            . "<td>".$dados->chamado."</td>"
                            . "<td> <a href='../solicitante_visualizar_chamado.php?id=".$dados->chamado."'>" . utf8_encode($dados->servicos_descricao). "</a></td>"
                            . "<td>" . utf8_encode($dados->funcionario) . "</td>";
                    
                    echo "</select></td>";    
                    
                }

            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    
    

}
