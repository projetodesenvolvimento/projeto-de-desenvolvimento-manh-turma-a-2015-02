<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");


class funcionarioDAO extends BancoPDO {

   
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

   
    public function inserir($funcionario) {

        try { 

            $stm = $this->conexao->prepare("INSERT INTO funcionarios (nome, matricula, senha, tipo) VALUES (?, ?, ?, ?)");
                        
            echo $funcionario->matricula;

            $stm->bindValue(1, $funcionario->nome);
            $stm->bindValue(2, $funcionario->matricula);
            $stm->bindValue(3, $funcionario->senha);
            $stm->bindValue(4, $funcionario->tipo);
                       
           
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ../../admin/formFuncionario.php");
                
            }

            
        
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

 
    public function visualizar($idfuncionario = "", $combo = "") {

        try { 

            if($idfuncionario == "") {
                $stm = $this->conexao->prepare("SELECT * FROM funcionarios");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM funcionarios WHERE idfuncionario = ?");
                $stm->bindParam(1, $idfuncionario, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                if($combo ==""){
                $tabela = "<table><tr>"
                        ."<td>IDFUNCIONARIO</td>"
                        ."<td>NOME</td>"
                        ."<td>MATRICULA</td>"
                        ."<td>SENHA</td>"
                        ."<td>TIPO</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idfuncionario."</td>"
                             ."<td>".$dados->nome."</td>"
                             ."<td>".$dados->matricula."</td>"
                             ."<td>".$dados->senha."</td>"
                             ."<td>".$dados->tipo."</td>"  
                             ."</tr>"; 
                }
            
                $tabela .= "</table>";
            
                echo $tabela;
            }else{
                
                $ops = "<option value='0'>Selecione um funcionario</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idfuncionario."'>".utf8_encode($dados->nome)."</option>";
                    }
                    echo $ops;
                
            }
        }
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }
    
    
    public function autenticar($funcionarios) {

        try { 

            $stm = $this->conexao->prepare("SELECT idfuncionario, tipo FROM funcionarios WHERE matricula = ? AND senha = ? AND (tipo = 2 OR tipo = 3)");
            
            $matricula = $funcionarios->matricula;
            $senha = $funcionarios->senha;
      
            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $matricula);
            $stm->bindValue(2, $senha);

            // realiza a execução do código na base
            if($stm->execute()) {
                $dados = $stm->fetch(PDO::FETCH_OBJ);
            }
            
            return $dados;
            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
            echo "Erro: ".$e->getMessage();
        }
    }
}
?>