<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

class DashboardDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    public function visualizarChamadoAberto() {
        try {
            $stm = $this->conexao->prepare("SELECT projeto.chamados.idchamado FROM projeto.chamados WHERE projeto.chamados.status_idstatus = 1");
            if ($stm->execute()) {
                $count = 0;
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                    $count+=1;
                }
            }
            return $count;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    public function visualizarChamadoFinalizado() {
        try {
            $stm = $this->conexao->prepare("SELECT projeto.chamados.idchamado FROM projeto.chamados WHERE projeto.chamados.status_idstatus = 4");
            if ($stm->execute()) {
                $count = 0;
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                    $count+=1;
                }
            }
            return $count;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    
    

    public function visualizarChamadoAndamento() {
        try {
            $stm = $this->conexao->prepare("SELECT projeto.chamados.idchamado FROM projeto.chamados WHERE projeto.chamados.status_idstatus = 2");
            if ($stm->execute()) {
                $count = 0;
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                    $count+=1;
                }
            }
            return $count;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    
    public function retornaNomeMes($mesNumero) {
        $nomeMes = "Mes";
        switch ($mesNumero) {
            case "01" : $nomeMes = "Janeiro"; 
                break;
            case "02" : $nomeMes = "Fevereiro";
                break;
            case "03" : $nomeMes = "Março"; 
                break;
            case "04" : $nomeMes = "Abril";
                break;
            case "05" : $nomeMes = "Maio"; 
                break;
            case "06" : $nomeMes = "Junho";
                break;
            case "07" : $nomeMes = "Julho"; 
                break;
            case "08" : $nomeMes = "Agosto";
                break;
            case "09" : $nomeMes = "Setembro"; 
                break;
            case "10" : $nomeMes = "Outubro";
                break;
            case "11" : $nomeMes = "Novembro"; 
                break;
            case "12" : $nomeMes = "Dezembro";
                break;
            default : $nomeMes = "Mes Invalido";
        }
        return $nomeMes;
    }

    public function visualizarMesAtual() {
        
       $count = 0;
       $meses = array();
       
        try {
            $stm = $this->conexao->prepare("SELECT * FROM projeto.chamadosaux WHERE projeto.chamadosaux.horainicio BETWEEN CURDATE() - INTERVAL 3 MONTH AND CURDATE();");
            
            if ($stm->execute()) {
                
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
      
                   $data = $dados->horainicio;
                   $data = explode('-', $data);
                   
                   
                   if($count == 0){
                       $meses[0] = $this->retornaNomeMes($data[1]);
                       $count++;
                   }
                   else if($count == 1){
                       if($meses[0] != $this->retornaNomeMes($data[1])){
                           $meses[1] = $this->retornaNomeMes($data[1]);
                           $count++;
                       }
                       
                   }
                   else if($count == 2){
                        if($meses[1] != $this->retornaNomeMes($data[1])){
                           $meses[2] = $this->retornaNomeMes($data[1]);
                           $count++;
                       }
                   }
                   
                   
                 
                }
                
                return $meses;
            }
           
            
        } catch (Exception $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    
    
    
    
   

    public function verificaNumeroRegistros() {
        try {
            $stm = $this->conexao->prepare("SELECT projeto.chamados.idchamado, projeto.servicos.descricao, projeto.status.descricao 
                        FROM projeto.chamados, projeto.servicos, projeto.status 
                        WHERE projeto.chamados.servicos_idservico = projeto.servicos.idservico 
                        AND projeto.chamados.status_idstatus = projeto.status.idstatus");

            if ($stm->execute()) {
                $count = 0;
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                    $count+=1;
                }
            }
            return $count;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    public function visualizarListaChamados() {
        try {
            $stm = $this->conexao->prepare("SELECT projeto.chamados.idchamado, projeto.servicos.descricao AS servico_descricao, projeto.status.descricao AS status_descricao
                        FROM projeto.chamados, projeto.servicos, projeto.status 
                        WHERE projeto.chamados.servicos_idservico = projeto.servicos.idservico 
                        AND projeto.chamados.status_idstatus = projeto.status.idstatus");

            if ($stm->execute()) {
                $tabela = "";
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                    if ($dados->status_descricao == "Em aberto") {
                        $tabela .= "<tr class='success'>"
                                . "<td>" . $dados->idchamado . "</td>"
                                . "<td>" . utf8_encode($dados->servico_descricao) . "</td>"
                                . "<td>" . $dados->status_descricao . "</td>"
                                . "</tr>";
                    } else if ($dados->status_descricao == "Em atendimento") {
                        $tabela .= "<tr class='warning'>"
                                . "<td>" . $dados->idchamado . "</td>"
                                . "<td>" . utf8_encode($dados->servico_descricao) . "</td>"
                                . "<td>" . $dados->status_descricao . "</td>"
                                . "</tr>";
                    } else {
                        $tabela .= "<tr class='danger'>"
                                . "<td>" . $dados->idchamado . "</td>"
                                . "<td>" . utf8_encode($dados->servico_descricao) . "</td>"
                                . "<td>" . $dados->status_descricao . "</td>"
                                . "</tr>";
                    }
                }
                echo $tabela;
            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

}
