<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");


class SolicitanteDAO extends BancoPDO {

   
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

   
    public function inserir($solicitante) {

        try { 

            $stm = $this->conexao->prepare("INSERT INTO solicitantes (idsolicitante, nome, matricula, senha, email,tipo) VALUES (?, ?, ?, ?, ?)");
                        
            echo $solicitante->matricula;

        
            $stm->bindValue(1, $solicitante->idsolicitante);
            $stm->bindValue(2, $solicitante->nome);
            $stm->bindValue(3, $solicitante->matricula);
            $stm->bindValue(4, $solicitante->senha);
            $stm->bindValue(5, $solicitante->email);
            $stm->bindValue(6, $solicitante->tipo);
                       
           
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ./index.php");
            }

            
        
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

 
    public function visualizar($idsolicitante = "", $combo = "") {

        try { 

            if($idsolicitante == "") {
                $stm = $this->conexao->prepare("SELECT * FROM solicitantes");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM solicitantes WHERE idsolicitante = ?");
                $stm->bindParam(1, $idsolicitante, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                if($combo ==""){
                $tabela = "<table><tr>"
                        ."<td>IDSOLICITANTES</td>"
                        ."<td>NOME</td>"
                        ."<td>MATRICULA</td>"
                        ."<td>SENHA</td>"
                        ."<td>EMAIL</td>"
						 ."<td>TIPO</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idsolicitante."</td>"
                             ."<td>".$dados->nome."</td>"
                             ."<td>".$dados->matricula."</td>"
                             ."<td>".$dados->senha."</td>"
                             ."<td>".$dados->email."</td>" 
			     ."<td>".$dados->tipo."</td>"  
                             ."</tr>"; 
                }
            
                $tabela .= "</table>";
            
                echo $tabela;
            }else{
                
                $ops = "<option value='0'>Selecione um solicitante</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idsolicitante."'>".$dados->matricula."</option>";
                    }
                    echo $ops;
                
            }
        }
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }
    
    public function autenticar($solicitantes) {

        try { 

            $stm = $this->conexao->prepare("SELECT idsolicitante, nome FROM solicitantes WHERE matricula = ? AND senha = ?");
            
            $matricula = $solicitantes->matricula;
            $senha = $solicitantes->senha;
      
            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $matricula);
            $stm->bindValue(2, $senha);

            // realiza a execução do código na base
            if($stm->execute()) {
                $dados = $stm->fetch(PDO::FETCH_OBJ);
            }
            
            return $dados;
            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
            echo "Erro: ".$e->getMessage();
        }
    }
    
   
}

?>