<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

// estabelecimento de herança
class AmbienteDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    // método de inserção de dados na base de dados
    // recebimento de objeto com atributos setados
    public function inserir($ambiente) {

        try { 

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO ambientes (idambiente, descricao, andares_idandar) VALUES (?, ?, ?)");
                        
            echo $ambiente->descricao;

            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $ambiente->idambiente);
            $stm->bindValue(2, $ambiente->descricao);
            $stm->bindValue(3, $ambiente->andares_idandar);
                       
            // realiza a execução do código na base
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ./index.php");
            }

            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

    // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    public function visualizar($idambiente = "", $combo = "", $filtro = "") {

        try { 
            
            if($filtro != "") {
                $stm = $this->conexao->prepare("SELECT * FROM ambientes WHERE andares_idandar = ?");
                $stm->bindParam(1, $filtro, PDO::PARAM_INT);  
            } else if($idambiente == "") {
                $stm = $this->conexao->prepare("SELECT * FROM ambientes");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM ambientes WHERE idambiente = ?");
                $stm->bindParam(1, $idambiente, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {   
                if($combo == "") 
                {    
                
                    // Para cada resultado encontrado...
                    $tabela = "<table><tr>"
                            ."<td>IDAMBIENTE</td>"
                            ."<td>DESCRIÇÃO</td>"
                            ."<td>ANDARES_IDANDAR</td>"
                            ."</tr>";

                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $tabela .= "<tr>"
                                 ."<td>".$dados->idambiente."</td>"
                                 ."<td>".$dados->descricao."</td>"
                                 ."<td>".$dados->andares_idandar."</td>"
                                 ."</tr>"; 
                    }
                
                    $tabela .= "</table>";
                    
                    echo $tabela;
                    
                } else {
                    
                    $ops = "<option value='0'>Selecione um ambiente</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idambiente."'>".$dados->descricao."</option>";
                    }
                    echo $ops;
                }
                
            }

        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }
    
    
}

?>