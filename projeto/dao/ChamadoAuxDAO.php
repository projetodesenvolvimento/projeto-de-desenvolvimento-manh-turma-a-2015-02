<?php
// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

class ChamadoAuxDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    public function inserir($chamadoAux) {

        try {

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO chamadosaux (descricao, horafim, principal, "
                    . "horainicio, imagem, chamados_idchamado, solicitantes_idsolicitante) "
                    . "VALUES (?,?,?,?,?,?,?)");

            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $chamadoAux->descricao);
            $stm->bindValue(2, $chamadoAux->horafim);
            $stm->bindValue(3, $chamadoAux->principal);
            $stm->bindValue(4, $chamadoAux->horainicio);
            $stm->bindValue(5, $chamadoAux->imagem);
            $stm->bindValue(6, $chamadoAux->chamados_idchamado);
            $stm->bindValue(7, $chamadoAux->solicitantes_idsolicitante);

            // realiza a execução do código na base
            $stm->execute();
            
            // tratamento de exceção nativo de PDO
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    public function visualizar($chamadoAux = "", $combo = "") {

        try {

            if ($chamadoAux == "") {
                $stm = $this->conexao->prepare("SELECT * FROM chamadosaux");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM chamadosaux WHERE idchamadoaux = ?");
                $stm->bindParam(1, $chamadoAux, PDO::PARAM_INT);
            }

            if ($stm->execute()) {
                if ($combo == "") {

                    while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                        $dados->idchamadoaux;
                        $dados->descricao;
                        $dados->horafim;
                        $dados->principal;
                        $dados->horainicio;
                        $dados->imagem;
                        $dados->chamados_idchamado;
                        $dados->solicitantes_idsolicitante;
                    }
                }
            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    public function listarChamadosSolicitante() {

        try {

                $stm = $this->conexao->prepare("SELECT chamadosaux.chamados_idchamado AS chamado,
                                                        servicos.descricao AS servicos_descricao,
                                                        (select descricao from status where idstatus = chamados.status_idstatus) AS status,
                                                        solicitantes.matricula AS matricula, 
                                                        (select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
                                                        andares.numandar AS andar, 
                                                        ambientes.descricao AS ambientes_descricao, 
                                                        setores.nomesetor AS setor,       
                                                        chamadosaux.descricao AS chamadosaux_descricao       
                                                        FROM solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados 
                                                        WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
                                                        AND chamadosaux.chamados_idchamado = chamados.idchamado
                                                        AND servicos.setores_idsetor = setores.idsetor
                                                        AND unidades.idunidade = andares.unidades_idunidade
                                                        AND ambientes.andares_idandar = andares.idandar
                                                        AND chamados.ambientes_idambiente = ambientes.idambiente
                                                        AND chamados.servicos_idservico = servicos.idservico
                                                        AND solicitantes.idsolicitante = ? ORDER BY chamadosaux.chamados_idchamado");
                
                $stm->bindValue(1, $_SESSION["idsolicitante"]);

            if ($stm->execute()) {

                $tabela = "";
                
                while ($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                    
                    if($dados->status == "Em aberto")
                        $sts = "danger";
                    else if ($dados->status == "Em atendimento")
                        $sts = "active";
                    else 
                        $sts = "success";
                  
                    $tabela .= "<tr class='".$sts."'>"
                            . "<td>".$dados->chamado."</td>"
                            . "<td><a href='solicitante_visualizar_chamado.php?id=".$dados->chamado."'>" . utf8_encode($dados->servicos_descricao) . "</a></td>"
                            . "<td>" . $dados->status . "</td></tr>";                    
                }

                echo $tabela;
            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    
    
    // retorna os dados de um chamado específico para que sejam mostrados no front-end de acordo com a necessidade 
    public function listarChamadoDetalhe($id = "") {

        try {

                $stm = $this->conexao->prepare("SELECT chamadosaux.chamados_idchamado AS chamado,
                                                    servicos.descricao AS servicos_descricao,
                                                    (select descricao from status where idstatus = chamados.status_idstatus) AS status,
                                                    solicitantes.matricula AS matricula, 
                                                    (select nomeunidade from unidades where unidades.idunidade = andares.unidades_idunidade) AS unidade, 
                                                    andares.numandar AS andar, 
                                                    ambientes.descricao AS ambientes_descricao, 
                                                    setores.nomesetor AS setor,       
                                                    chamadosaux.descricao AS chamadosaux_descricao       
                                                    from solicitantes, unidades, andares, ambientes, setores, servicos, chamadosaux, chamados 
                                                    WHERE chamadosaux.solicitantes_idsolicitante = solicitantes.idsolicitante
                                                    AND chamadosaux.chamados_idchamado = chamados.idchamado
                                                    AND servicos.setores_idsetor = setores.idsetor
                                                    AND unidades.idunidade = andares.unidades_idunidade
                                                    AND ambientes.andares_idandar = andares.idandar
                                                    AND chamados.ambientes_idambiente = ambientes.idambiente
                                                    AND chamados.servicos_idservico = servicos.idservico
                                                    AND projeto.solicitantes.idsolicitante = ?
                                                    AND chamadosaux.chamados_idchamado = ?");
                
                $stm->bindValue(1, $_SESSION["idsolicitante"]);
                $stm->bindValue(2, $id);

            if ($stm->execute()) {
                return $stm->fetch(PDO::FETCH_OBJ);
            }
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

}
