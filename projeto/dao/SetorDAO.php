<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

// estabelecimento de herança
class SetorDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    // método de inserção de dados na base de dados
    // recebimento de objeto com atributos setados
    public function inserir($setor) {

        try { 

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO setores (idsetor, nomesetor) VALUES (?, ?)");
                        
            echo $setor->nomesetor;

            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $setor->idsetor);
            $stm->bindValue(2, $setor->nomesetor);
            
                       
            // realiza a execução do código na base
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ./index.php");
            }

            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

    // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    public function visualizar($idsetor = "", $combo = "") {

        try { 

            if($idsetor == "") {
                $stm = $this->conexao->prepare("SELECT * FROM setores");
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM setores WHERE idsetor = ?");
                $stm->bindParam(1, $idsetor, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                if($combo == ""){
                
                // Para cada resultado encontrado...
                $tabela = "<table><tr>"
                        ."<td>IDSETORES</td>"
                        ."<td>NOME DO SETOR</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idsetor."</td>"
                             ."<td>".$dados->nomesetor."</td>"
                             ."</tr>"; 
                }
                
                $tabela .= "</table>";
            
                echo $tabela;
                }else{
                    
                    $ops = "<option value='0'>Selecione um Setor</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idsetor."'>".utf8_encode($dados->nomesetor)."</option>";
                    }
                    echo $ops;
                }
            }

        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }    
    
}

?>