<?php

// incluir a classe para estabelecimento de herança e utilização de método de conexão
require_once("BancoPDO.php");

// estabelecimento de herança
class AndarDAO extends BancoPDO {

    // no construtor chamada de método de conexão da superclasse e realização de conexão
    public function __construct() {
        $this->conexao = BancoPDO::conexao();
    }

    // método de inserção de dados na base de dados
    // recebimento de objeto com atributos setados
    public function inserir($andar) {

        try { 

            // preparação de statement para execução de SQL na base
            // casa interrogação corresponde a um valor que será recebido posteriormente
            $stm = $this->conexao->prepare("INSERT INTO andares (idandar, numandar, unidades_idunidade) VALUES (?, ?, ?)");
                        
            echo $andar->numandar;

            // passagem de valores na ordem correta de entrada
            // corresponde a cada ponto de interrogação na SQL do statement
            $stm->bindValue(1, $andar->idandar);
            $stm->bindValue(2, $andar->numandar);
            $stm->bindValue(3, $andar->unidades_idunidade);
            
            
                       
            // realiza a execução do código na base
            if($stm->execute()) {
                echo "Dados inseridos com sucesso! <br/>";
                header("Location: ./index.php");
            }

            
        // tratamento de exceção nativo de PDO
        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }
    }

    // método de visualização de dados na base de dados
    // opcionalmente permite o recebimento de um valor de id para filtro
    public function visualizar($idandar = "", $combo = "", $filtro = "") {

        try { 

            if($filtro != "") {
                $stm = $this->conexao->prepare("SELECT * FROM andares WHERE unidades_idunidade = ?");
                $stm->bindParam(1, $filtro, PDO::PARAM_INT);     
            } else if($idandar == "") {
                $stm = $this->conexao->prepare("SELECT * FROM andares");  
            } else {
                $stm = $this->conexao->prepare("SELECT * FROM andares WHERE idandar = ?");
                $stm->bindParam(1, $idandar, PDO::PARAM_INT);
            }

            if($stm->execute()) 
            {
                if($combo == ""){
                
                // Para cada resultado encontrado...
                $tabela = "<table><tr>"
                        ."<td>IDANDAR</td>"
                        ."<td>NÚMERO DO ANDAR</td>"
                        ."<td>ID DA UNIDADE</td>"
                        ."</tr>";
            
                while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                   $tabela .= "<tr>"
                             ."<td>".$dados->idandar."</td>"
                             ."<td>".$dados->numandar."</td>"
                             ."<td>".$dados->unidades_idunidade."</td>"
                             ."</tr>"; 
                }
                
                $tabela .= "</table>";
            
                echo $tabela;
                
                 }else{
                
                     $ops = "<option value='0'>Selecione um Andar</option>";
                    while($dados = $stm->fetch(PDO::FETCH_OBJ)) {
                       $ops .= "<option value='".$dados->idandar."'>".$dados->numandar."</option>";
                    }
                    return $ops;
                
            }
                   
            }          

        } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
        }

    }
    
    
}

?>