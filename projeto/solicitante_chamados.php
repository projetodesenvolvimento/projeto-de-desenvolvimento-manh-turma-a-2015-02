<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "cabecalho.php";
?>

<main>
    <h1>Solicitações realizadas</h1>
    <!-- Formulários -->
    <form class="form-horizontal">

        <table class="table table-hover" style="width: 80%; margin: auto;">
            <thead class="theads">
                <tr>
                    <td>Código</td>
                    <td>Serviço</td>
                    <td>Status</td>
                </tr>
            </thead>
            <tbody>

                <?php
                require 'includes/validacao.php';
                include 'dao/ChamadoAuxDAO.php';
                $mostrar = new ChamadoAuxDAO();
                echo $mostrar->listarChamadosSolicitante();
                ?>        


            </tbody>
        </table>
    </form>
</main>

<?php        
// inclusão do rodapé com toda fechamento da estrutura HTML e chamadas de JS
include "rodape.php";
?>