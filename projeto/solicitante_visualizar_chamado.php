<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "cabecalho.php";
?>


<link rel="stylesheet" href="css/normalize.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/examples.css">

<link rel="stylesheet" href="css/themes/bars-1to10.css">
   

<?php
require 'includes/validacao.php';
include 'dao/ChamadoAuxDAO.php';

$id = $_REQUEST["id"];

$mostrar = new ChamadoAuxDAO();
$dados = $mostrar->listarChamadoDetalhe($id);
         
?>

<main>
    <h1>Visualizar dados da solicitação</h1>
    
    <form class="form-horizontal">
        <!-- Código -->
        <div class="form-group" id="formcolor">
            <div class="form-group">
                <label class="col-md-2 control-label">Código</label>
                <div class="col-md-4">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                             <?php echo $dados->chamado; ?> 
                        </div>
                    </div>
                </div>
                <label  class="col-md-1 control-label">Status</label>
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <?php echo $dados->status; ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Unidade / Andar -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Unidade / Andar</label>
                <div class="col-md-4">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                           <?php echo $dados->unidade."/".$dados->andar ?> 
                        </div>
                    </div>
                </div>
                <label  class="col-md-1 control-label">Ambiente</label>
                <div class="col-md-4">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                           <?php echo utf8_encode($dados->ambientes_descricao); ?>
                        </div>
                    </div>
                </div>
            </div>

           
            <!-- Setor -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Setor</label>
                <div class="col-md-4">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                           <?php echo utf8_encode($dados->setor); ?>
                        </div>
                    </div>
                </div>
                <label  class="col-md-1 control-label">Serviço</label>
                <div class="col-md-4">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <?php echo utf8_encode($dados->servicos_descricao); ?>
                        </div>
                    </div>
                </div>
            </div>
          
            <!-- Descrição -->
            <div class="form-group">
                <label  class="col-md-2 control-label">Descrição</label>
                <div class="col-md-9">
                   <div class="panel panel-warning">
                        <div class="panel-body">
                           <?php echo utf8_encode($dados->chamadosaux_descricao); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        
    <?php
        if($dados->status == "Finalizado") {  
    ?>
      <!-- Avaliacao -->
      <div id="formcolor">
            <label  class="col-md-2 control-label">Avaliação</label>
            <div class="form-group">
                <div class="col-md-1" style="padding-top: 10px; width: 200px;">
                    <div class="br-wrapper br-theme-bars-1to10">
                        <select id="example-1to10" name="rating">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <label  class="col-md-1 control-label">Comentário</label>
                <div class="col-md-5 col-xs-5">
                    <textarea name="descricao" class="form-control" maxlength="140"></textarea>
                </div>
                <div class="col-md-2 col-xs-2">
                    <button type="submit" class="btn btn-lg btn-warning">ENVIAR</button>
                </div>
            </div>

           
        </div>
    </form>
    
    <?php } ?>
    
        
    </form>
    
    
    
</main>    

<?php        
// inclusão do rodapé com toda fechamento da estrutura HTML e chamadas de JS
include "rodape.php";
?>

<script src="js/jquery.barrating.js"></script>
     <script src="js/examples.js"></script>

     <script src="jquery.barrating.min.js"></script>
     <script type="text/javascript">
         $(function () {
             $('example-1to10').barrating('show', {
                 theme: 'bars-1to10'
             });
         });
</script> 