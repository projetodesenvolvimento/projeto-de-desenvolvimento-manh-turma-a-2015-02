<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "cabecalho.php";
?>
        <?php
            include "../dao/DashboardDAO.php";
            
            // Cria objeto DAO da Dashboard
            $dashboard = new DashboardDAO ();
            $dashboardaux = new DashboardDAO ();
            
            //Captura os status dos chamados
            $dash_chamado_aberto = $dashboard->visualizarChamadoAberto();
            $dash_chamado_finalizado = $dashboard->visualizarChamadoFinalizado();
            $dash_chamado_andamento = $dashboard->visualizarChamadoAndamento();
            
            // Verifica a quantidade de registros no banco
            $qtd_registros = $dashboard->verificaNumeroRegistros();
            $meses = $dashboardaux->visualizarMesAtual();
          
            
        ?>

        <script type="text/javascript">
            // Gráfico de pizza
            google.load("visualization", "1", {packages: ["corechart"]});
            google.setOnLoadCallback(drawChartPie);
            function drawChartPie() {

                var data = google.visualization.arrayToDataTable([
                    ['Status', 'Quantidade'],
                    ['Aberto', <?php echo $dash_chamado_aberto; ?>],
                    ['Em atendimento', <?php echo $dash_chamado_andamento; ?>],
                    ['Fechado', <?php echo $dash_chamado_finalizado; ?>]

                ]);

                var optionsPie = {
                    title: '',
                    backgroundColor: {fill: 'transparent'}
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, optionsPie);
            }

            // Gráfico de barra
            google.load("visualization", "1.1", {packages: ["bar"]});
            google.setOnLoadCallback(drawChartBar);
            function drawChartBar() {
                var data = google.visualization.arrayToDataTable([
                    ['Mês', 'Abertos', 'Fechados'],
                    ['<?php echo $meses[0]; ?>', <?php echo $dash_chamado_aberto . "," . $dash_chamado_finalizado; ?>],
                    ['<?php echo $meses[1]; ?>', <?php echo $dash_chamado_aberto . "," . $dash_chamado_finalizado; ?>],
                    ['<?php echo $meses[2]; ?>', <?php echo $dash_chamado_aberto . "," . $dash_chamado_finalizado; ?>]
                ]);

                var optionsBar = {
                    backgroundColor: 'transparent',
                    chart: {
                        title: '',
                        subtitle: '',
                    }

                };

                var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                chart.draw(data, google.charts.Bar.convertOptions(optionsBar));
            }
        </script>
       

        <main>
            <h1>Dashboard</h1>
            <!-- Gráfico pizza -->
            
            <div class="col-md-6" style="background: rgba(255, 255, 255, 0.8); width: 48%; margin-left: 15px; padding-top: 10px;">
                <h6>Chamados por status de atendimento</h6>
                <div id="piechart" style="width: 100%; height: 100%;"></div>
            </div>
            <!-- Gráfico barra -->
            <div class="col-md-6" style="background: rgba(255, 255, 255, 0.8); width: 50%; margin-bottom: 20px; padding-top: 10px;">
                <h6>Chamados nos últimos meses</h6>
                <div id="columnchart_material" style="width: 100%; height: 100%;"></div>
            </div>
            <!-- Tabela de chamados -->
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead style="color: #000; font-weight: bold; background-color: #eeeeee;">
                        <tr>
                            <td>Código</td>
                            <td>Serviço</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $dashboard->visualizarListaChamados();
                        ?>
                    </tbody>
                </table>
            </div>
        </main>

<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "rodape.php";
?>