<?php
require "../includes/validacao.php";
require '../dao/funcionarioDAO.php';
include '../dao/AlocacaoDAO.php';

include "cabecalho.php";
?>

    <main>
        <h1>Solicitações Abertas</h1>
        <!-- Formulários -->
        <div class="form-horizontal" >

            <table class="table table-hover" style="width:98%; margin: auto;">
                <thead class="theads">
                    <tr>
                        <td>Código</td>
                        <td>Serviço</td>
                        <td>Funcionário</td>
                        <td>Ação</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $mostrar = new AlocacaoDAO();
                    echo $mostrar->listarLocacoesAbertas();
                    ?>                        
                </tbody>
            </table>
        </div> 
    </main>

    <main>
        <h1>Solicitações Em Atendimento</h1>
        <!-- Formulários -->
        <div class="form-horizontal" >

            <table class="table table-hover"  style="width:98%; margin: auto;">
                <thead class="theads">
                    <tr>
                        <td>Código</td>
                        <td>Serviço</td>
                        <td>Funcionário</td>
                        <td>Ação</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $mostrar = new AlocacaoDAO();
                    echo $mostrar->listarLocacoesAndamento();
                    ?>                        
                </tbody>
            </table>
        </div>    
        </main>

        <h1>Solicitações Finalizadas</h1>
        <!-- Formulários -->
        <div class="form-horizontal" >

            <table class="table table-hover"  style="width:98%; margin: auto;">
                <thead class="theads">
                    <tr>
                        <td>Código</td>
                        <td>Serviço</td>
                        <td>Funcionário</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $mostrar = new AlocacaoDAO();
                    echo $mostrar->listarLocacoesFechadas();
                    ?>                        
                </tbody>
            </table>
        </div>    
</main>

<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "rodape.php";
?>