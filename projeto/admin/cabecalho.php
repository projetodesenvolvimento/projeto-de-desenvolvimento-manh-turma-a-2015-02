<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Posso Ajudar?</title>
        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../css/nivo-lightbox.css" rel="stylesheet" />
        <link href="../css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
        <link href="../css/animate.css" rel="stylesheet" />
        <!-- Squad theme CSS -->
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/color/default.css" rel="stylesheet">
        
        <!-- Gráficos  -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    </head>

    <body data-spy="scroll">

        <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">

                                <li>
                                    <a href="formFuncionario.php" class="gn-icon gn-icon-download">Funcionários</a>
                                </li>
                                <li><a href="formLocacao.php" class="gn-icon gn-icon-cog">Painel de Chamados</a></li>

                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
                <li><a href="gerencia_dashboard.php">Dashboard</a></li>

            </ul>
        </div>

        <!-- Section: intro -->
        <section id="intro" class="intro" >
            <div class="slogan">
               
                    
                 