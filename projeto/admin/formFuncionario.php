<?php
require "../includes/validacao.php";
include "cabecalho.php";
?>

    <main>
        <h1>CADASTRO DE FUNCIONÁRIOS</h1>
        <!-- Formulários -->
        <form class="form-horizontal" action="../admin/cadastros/cadastrarfuncionarios.php" method="post">
            <!-- Nome -->
            <div id="formcolor">
                <div class="form-group">
                    <label class="col-md-2 control-label">Nome</label>
                    <div class="col-md-8">

                        <input type="text" id="nome" name="nome" class="form-control" >

                    </div>
                </div>

                <!-- Senha -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Senha</label>
                    <div class="col-md-8">

                        <input type="password" name="senha" id="senha" class="form-control" >

                    </div>
                </div>

                <!-- Matricula -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Matricula</label>
                    <div class="col-md-8">

                        <input type="text" id="matricula" name="matricula" class="form-control" >

                    </div>
                </div>
                <!-- Tipo -->
                <div class="form-group">
                    <label  class="col-md-2 control-label">Tipo</label>
                    <div class="col-md-8">

                        <input type="text" id="tipo" name="tipo" class="form-control" >

                    </div>
                </div>                            
            </div>
            <button type="submit" class="form-group btn btn-warning">ENVIAR</button>
        </form>
    </main>
        
<?php
// inclusão do cabeçalho com toda estrutura HTML inicial e chamadas de CSS e JS
include "rodape.php";
?>
