﻿<?php
// verifica se as informações vieram 
// através do método POST
if (isset($_POST)) {
    include "../../classes/Andar.php";
    include "../../dao/AndarDAO.php";

    // recebe os valores vindos do formulário através de post
    $numandar = $_POST["numandar"];
    $idunidade = $_POST["idunidade"];
    
    $andar = new Andar("", $numandar, $idunidade);

    $acoes = new AndarDAO();
// chama o método inserir passando o objeto com dados de usuário
    $acoes->inserir($andar);
}
?>