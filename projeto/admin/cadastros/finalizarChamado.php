<?php
require '../../includes/validacao.php';
include '../../dao/ChamadoDAO.php';

if (isset($_POST)) {
    // recebe os valores vindos do formulário através de post

    $idchamado = $_POST["idchamado"];

    $chamado = new ChamadoDAO();
    if($chamado->trocaStatus($idchamado, 4))
        header("Location: ../formLocacao.php");
}

