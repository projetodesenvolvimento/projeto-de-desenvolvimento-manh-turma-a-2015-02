﻿<?php
// verifica se as informações vieram 
// através do método POST
if (isset($_POST)) {
    include "../../classes/Servico.php";
    include "../../dao/ServicoDAO.php";

    // recebe os valores vindos do formulário através de post
    $sla = $_POST["sla"];
    $grauseveridade = $_POST["grauseveridade"];
    $descricao = $_POST["descricao"];
    $idsetor = $_POST["idsetor"];

    $servico = new Servicos("", $sla, $grauseveridade, $descricao, $idsetor);

    $acoes = new ServicosDAO();
    // chama o método inserir passando o objeto com dados de usuário
    $acoes->inserir($servico);
}
?>