<?php
// verifica se as informações vieram 
// através do método POST
if (isset($_POST)) {
    include "../../classes/Funcionario.php";
    include "../../dao/FuncionarioDAO.php";

    // recebe os valores vindos do formulário através de post
    $nome = $_POST["nome"];
    $senha = $_POST["senha"];
    $matricula = $_POST["matricula"];
    $tipo = $_POST["tipo"];
    

    $funcionario = new Funcionario("", $nome, $matricula, $senha, $tipo);
    
    $acoes = new FuncionarioDAO();
    // chama o método inserir passando o objeto com dados de usuário
    $acoes->inserir($funcionario);
    
    
}
?>