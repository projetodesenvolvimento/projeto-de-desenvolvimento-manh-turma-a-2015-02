<?php
require '../../includes/validacao.php';

// verifica se as informações vieram 
// através do método POST

if (isset($_POST)) {
    
    include '../../classes/Alocacao.php';
    include '../../dao/AlocacaoDAO.php';
    include '../../dao/ChamadoDAO.php';
    
    // recebe os valores vindos do formulário através de post
    
    $idfuncionario = $_POST["idfuncionario"];
    $idchamado = $_POST["idchamado"];
    
    // insere a alocação 
    $locacao = new Alocacao($idchamado, $idfuncionario);
    $acao = new AlocacaoDAO();
    
    // se a alocação for realizada com sucesso, então muda o status do chamado
    if($acao->inserir($locacao)) {
        
        $chamado = new ChamadoDAO();
        if($chamado->trocaStatus($idchamado, 2))
            header("Location: ../formLocacao.php");
        
    }
    
    
    
    
}