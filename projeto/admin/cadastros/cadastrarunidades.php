﻿<?php
// verifica se as informações vieram 
// através do método POST
if (isset($_POST)) {
    include "../../classes/Unidade.php";
    include "../../dao/UnidadeDAO.php";

    // recebe os valores vindos do formulário através de post
    $numunidade = $_POST["numunidade"];
    $nomeunidade = $_POST["nomeunidade"];
        
    $unidade = new Unidade("", $numunidade, $nomeunidade);

    $acoes = new UnidadeDAO();
// chama o método inserir passando o objeto com dados de usuário
    $acoes->inserir($unidade);
}
?>