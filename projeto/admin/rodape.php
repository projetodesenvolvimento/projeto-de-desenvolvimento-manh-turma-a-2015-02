
                    
                    
                </main>
        </section>
     
        <!-- Section: contact -->
        <section id="contact" class="home-section text-center">

            <div class="container">

                <div class="row">
                    <div class="col-lg-8 col-md-offset-2">

                        <div class="widget-contact row">
                            <div class="col-lg-6">
                                <address>
                                    <strong>Faculdade Senac Ltd.</strong><br>
                                    Coronel Genuino, 358 <br>
                                    Porto Alegre, RS <br>
                                    <abbr title="Phone">P:</abbr> (051) 3212-4444
                                </address>
                            </div>

                            <div class="col-lg-6">
                                <address>
                                    <strong>Email</strong><br>
                                    <a href="mailto:#">atendimentofatecpoa@senacrs.com.br</a><br />

                                </address>	

                            </div>
                        </div>	
                    </div>

                </div>	

            </div>
        </section>
        <!-- /Section: contact -->

        <footer id="footercolor">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <p>Copyright &copy; 2015 Diego Massimino</p>
                    </div>
                    <!-- 
                        All links in the footer should remain intact. 
                        Licenseing information is available at: http://bootstraptaste.com/license/
                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Ninestars
                    -->
                </div>	
            </div>
        </footer>
        
        <!-- Core JavaScript Files -->
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.easing.min.js"></script>	
        <script src="../js/classie.js"></script>
        <script src="../js/gnmenu.js"></script>
        <script src="../js/jquery.scrollTo.js"></script>
        <script src="../js/nivo-lightbox.min.js"></script>
        <script src="../js/stellar.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="../js/custom.js"></script>

    </body>

</html>
