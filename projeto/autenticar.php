<?php

// verifica se o botão acao de login foi acionado
if(isset($_POST["acao"])) {

    $matricula = $_POST["matricula"];
    $senha = $_POST["senha"];
    
    if($_POST["op"] == "admin") {
        include "classes/Funcionario.php"; // incluir chamada para classe Funcionario
        include "dao/funcionarioDAO.php"; // incluir chamada para classe FuncionarioDAO
        $obj = new Funcionario("", "", $matricula, $senha, "");  //instancia classe Funcionario passando dados por parâmetro
        $dao = new funcionarioDAO();
    } else {
        include "classes/Solicitante.php"; // incluir chamada para classe Solicitante
        include "dao/SolicitanteDAO.php"; // incluir chamada para classe SolicitanteDAO
        $obj = new Solicitante("","",$matricula, $senha, "", "");  //instancia classe Solicitante passando dados por parâmetro
        $dao = new SolicitanteDAO();
    }
    
    if(($retorno = $dao->autenticar($obj)) == null) {
        echo "Matrícula e/ou Senha Inválida.";
        header("refresh:3; url=index.php");
    } else {
        // cria a sessão
        session_start();
        // d = dia
        // m = mês
        // y = ano com 2 dígitos ou Y ano 4 dígitos
        // h = hora padrão 12 ou H = hora padrão 24
        // i = minutos
        // s = segundos
        $_SESSION["datahora"] = date("d/m/Y H:i:s");
        $_SESSION["id"] = session_id();
        $_SESSION["nome"] = $retorno->nome;
        
        if($_POST["op"] == "admin") {
            $_SESSION["idfuncionario"] = $retorno->idfuncionario;    
            header("Location: admin/gerencia_dashboard.php");
        } else {
            $_SESSION["idsolicitante"] = $retorno->idsolicitante;
            header("Location: solicitante_abrir.php");
        }
            
        	
    }

}

?>